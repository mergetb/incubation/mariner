package mariner

import (
	"fmt"
	"math/rand"
	"net"
	"os/exec"
	"time"

	api "gitlab.com/mergetb/tech/mariner/api"
)

func rand_init() {
	rand.New(rand.NewSource(time.Now().UnixNano()))
}

func NewQCmd(vm *api.VMSpec) *exec.Cmd {

	var parts []string
	parts = append(parts, qname(vm)...)
	parts = append(parts, qmachine(vm)...)
	parts = append(parts, qmemory(vm)...)
	parts = append(parts, qprocs(vm)...)
	parts = append(parts, qeths(vm)...)
	parts = append(parts, qdisplay(vm)...)
	parts = append(parts, qbootdisk(vm)...)
	parts = append(parts, qextra(vm)...)

	return exec.Command(emulator(vm), parts...)

}

func emulator(vm *api.VMSpec) string {
	return QemuEmulator
}

func qname(vm *api.VMSpec) []string {
	return []string{
		"-name",
		fmt.Sprintf("guest=%s", vm.Name),
	}
}

func qmachine(vm *api.VMSpec) []string {
	return []string{
		"-machine",
		fmt.Sprintf("%s,accel=kvm", QemuMachine),
	}
}

func qmemory(vm *api.VMSpec) []string {

	var m uint64
	for _, dimm := range vm.Dimms {
		m += dimm.Capacity
	}
	m = uint64(float64(m) * 1e-6) // to megabytes

	// 1GB min
	if m < 1024 {
		m = 1024
	}

	return []string{
		"-m",
		fmt.Sprintf("%d", m),
	}

}

func qprocs(vm *api.VMSpec) []string {

	if len(vm.Cpus) == 0 {
		return []string{
			"-smp",
			"sockets=1,cores=1,threads=1",
		}
	}

	//TODO disregards heterogeneus socket configs
	return []string{
		"-smp",
		fmt.Sprintf("%d,sockets=%d,cores=%d,threads=1",
			len(vm.Cpus)*int(vm.Cpus[0].Cores),
			len(vm.Cpus),
			vm.Cpus[0].Cores,
		),
	}

}

func qeths(vm *api.VMSpec) []string {

	var parts []string
	var i int
	for _, n := range vm.Nics {
		parts = append(parts, nic_qeths(i, n)...)
		i += len(n.Ports)
	}

	return parts

}

func nic_qeths(start int, nic *api.EtherNic) []string {

	var parts []string
	for i, p := range nic.Ports {
		parts = append(parts, qeth(start+i, p, nic)...)
	}

	return parts

}

func qeth(i int, p *api.EtherPort, nic *api.EtherNic) []string {

	parts := []string{
		"-device",
		fmt.Sprintf(
			"virtio-net-pci,ioeventfd=on,netdev=host%d,id=net%d,mac=%s",
			i,
			i,
			genMac().String(),
		),
		"-netdev",
		fmt.Sprintf(
			"tap,id=host%d,ifname=tap%d,vhost=on,script=no,downscript=no",
			i,
			p.Ifx.Index,
		),
	}

	return parts
}

func qdisplay(vm *api.VMSpec) []string {
	return []string{
		"-device", "cirrus-vga,id=video0",
		"-vnc",
		fmt.Sprintf("%s:%d", VncListen, VncStart+int(vm.Index)),
	}
}

func qbootdisk(vm *api.VMSpec) []string {

	//TODO user spec
	return []string{
		"-device", "virtio-blk-pci,scsi=off,drive=disk0,bootindex=1,write-cache=on",
		"-drive", fmt.Sprintf(
			"file=/var/mariner/bootdisk/%s,format=qcow2,if=none,id=disk0,cache=none",
			vm.Name,
		),
	}
}

func qextra(vm *api.VMSpec) []string {

	return []string{
		"-daemonize",
		"-nodefaults",
		"-pidfile", fmt.Sprintf("/var/mariner/pid/%s", vm.Name),
	}

}

func genMac() net.HardwareAddr {
	// mariner mac address structure
	// cc:17:01:00:00:00

	n := rand.Uint32()

	return net.HardwareAddr{
		0xCC,
		0x17,
		0x01,
		byte((n & 0xFF0000) >> 16),
		byte((n & 0x00FF00) >> 8),
		byte(n & 0x0000FF),
	}

}
