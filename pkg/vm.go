package mariner

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/mariner/api"
)

func CreateVMs(vms []*api.VMSpec) error {

	var instances InstanceInfos

	err := validateVmSpecs(vms)
	if err != nil {
		return err
	}

	// on failure destroy all instances created and bail
	onFailure := func() {
		errs := destroyAll(instances)
		for _, e := range errs {
			log.WithError(e).Error("error deleting instance")
		}
	}

	// create virtual machines
	for _, vm := range vms {

		instance, err := create(vm)
		if err != nil {
			onFailure()
			return err
		}
		instances = append(instances, instance)

	}

	// save instance info
	err = instances.Save()
	if err != nil {
		onFailure()
		return err
	}

	return nil

}

func DestroyVMs(vms []string) error {

	var errs []error

	var instances InstanceInfos
	for _, vm := range vms {
		i, err := GetInstance(vm)
		if err != nil {
			return err
		}
		if i == nil {
			errs = append(errs, fmt.Errorf("%s not found", vm))
			continue
		}
		instances = append(instances, i)
	}

	errs = append(errs, destroyAll(instances)...)

	if len(errs) == 0 {
		err := instances.Delete()
		if err != nil {
			errs = append(errs, err)
		}
	}

	return coalesce(errs)

}

func ListVMs() ([]*api.VMSpec, error) {

	instances, err := listInstances()
	if err != nil {
		return nil, err
	}

	result := make([]*api.VMSpec, len(instances))
	for i, instance := range instances {
		result[i] = instance.Spec
	}

	return result, nil

}

func validateVmSpecs(vms []*api.VMSpec) error {

	var errs []error
	for _, vm := range vms {
		err := validateVmSpec(vm)
		if err != nil {
			errs = append(errs, err)
		}
	}

	return coalesce(errs)

}

func validateVmSpec(vm *api.VMSpec) error {

	if vm.Name == "" {
		return fmt.Errorf("missing vm name")
	}

	return nil

}
