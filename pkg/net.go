package mariner

import (
	"fmt"
	"os/exec"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/mariner/api"
	"gitlab.com/mergetb/tech/rtnl"
)

func NetInit() {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Fatal("error opening rtnl default context")
	}

	// ensure mariner bridge is a thing

	mbr := &rtnl.Link{
		Info: &rtnl.LinkInfo{
			Name: "mbr",
			Bridge: &rtnl.Bridge{
				VlanAware: true,
			},
		},
	}

	err = mbr.Present(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure mbr presence")
	}

	err = mbr.Up(ctx)
	if err != nil {
		log.WithError(err).Fatal("failed to ensure mbr up")
	}

	// allow forwarding over the mbr

	err = exec.Command(
		"iptables", "-C", "FORWARD", "-i", "mbr", "-j", "ACCEPT",
	).Run()
	if err != nil {

		out, err := exec.Command(
			"iptables", "-I", "FORWARD", "-i", "mbr", "-j", "ACCEPT",
		).CombinedOutput()

		if err != nil {
			log.
				WithError(err).
				WithFields(log.Fields{"out": string(out)}).
				Fatal("failed to setup bridge filtering rule")
		}

	}

}

func createVmInterfaces(vm *api.VMSpec) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Error("error opening rtnl default context")
		return err
	}

	mbr, err := rtnl.GetLink(ctx, "mbr")
	if err != nil {
		log.WithError(err).Error("error getting mbr handle")
		return err
	}

	var errs []error
	for _, nic := range vm.Nics {
		for _, port := range nic.Ports {

			name := fmt.Sprintf("tap%d", port.Ifx.Index)
			fields := log.Fields{
				"machine": vm.Name,
				"tap":     name,
			}
			log.WithFields(fields).Debug("creating tap")

			out, err := exec.Command(
				"ip", "tuntap", "add", "dev", name, "mode", "tap",
			).CombinedOutput()
			if err != nil {
				fields["out"] = string(out)
				log.WithError(err).WithFields(fields).Error("error creating tap")
				errs = append(errs, err)
			}

			// the tun/tap interface is a bit on the stupid side, need to create
			// through specific tun/tap interface and then manage through netlink

			link := &rtnl.Link{
				Info: &rtnl.LinkInfo{
					Name: name,
				},
			}

			err = link.SetMaster(ctx, int(mbr.Msg.Index))
			if err != nil {
				log.WithError(err).WithFields(fields).Error("error setting tap bridge")
				errs = append(errs, err)
			}

			if port.Ifx.Vlan != 0 {
				err = link.SetUntagged(ctx, uint16(port.Ifx.Vlan), false, true, false)
				if err != nil {
					log.WithError(err).WithFields(fields).Error("error tagging tap")
				}
			}

			err = link.Up(ctx)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"machine": vm.Name,
					"tap":     name,
				}).Error("error bringing tap up")
				errs = append(errs, err)
			}

		}
	}

	return coalesce(errs)

}

func deleteVmInterfaces(vm *api.VMSpec) error {

	ctx, err := rtnl.OpenDefaultContext()
	if err != nil {
		log.WithError(err).Error("error opening rtnl default context")
		return err
	}

	var errs []error
	for _, nic := range vm.Nics {
		for _, port := range nic.Ports {

			name := fmt.Sprintf("tap%d", port.Ifx.Index)

			// the tun/tap interace is a bit on the stupid side, need to
			// delete tun/tap with netlink before deleting it through tun/tap
			// interface, if one is done but not the other, tun/tap indexing gets
			// fucked up
			link := &rtnl.Link{
				Info: &rtnl.LinkInfo{
					Name: name,
				},
			}
			err := link.Absent(ctx)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"machine": vm.Name,
					"tap":     name,
				}).Error("error deleting tap interface")
			}

			out, err := exec.Command(
				"ip", "tuntap", "delete", "dev", name, "mode", "tap",
			).CombinedOutput()
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"machine": vm.Name,
					"tap":     name,
					"out":     string(out),
				}).Error("error deleting tap")
				errs = append(errs, err)
			}

		}
	}

	return coalesce(errs)

}
