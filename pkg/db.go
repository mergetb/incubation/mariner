package mariner

import (
	"bytes"
	"encoding/json"
	"fmt"
	"os"
	"sync"

	"github.com/golang/protobuf/jsonpb"
	log "github.com/sirupsen/logrus"
	bolt "go.etcd.io/bbolt"

	"gitlab.com/mergetb/tech/cogs/pkg"
	api "gitlab.com/mergetb/tech/mariner/api"
)

var (
	ifctrMtx sync.Mutex
	vmctrMtx sync.Mutex
	cbucket  = []byte("counters")
	ibucket  = []byte("instances")
	vmctr    = []byte("vm")
	ifxctr   = []byte("ifx")
)

const (
	dbfile = "/var/mariner/db"
)

func DbInit() {

	err := os.MkdirAll("/var/mariner", 0644)
	if err != nil {
		log.WithError(err).Fatal("failed to create /var/mariner directory")
	}

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Fatal("failed to open db")
	}
	defer db.Close()

	err = db.Update(func(tx *bolt.Tx) error {

		cb, err := initBucket(cbucket, tx)
		if err != nil {
			return err
		}

		_, err = initBucket(ibucket, tx)
		if err != nil {
			return err
		}

		err = initCounter(ifxctr, MaxIfx, cb)
		if err != nil {
			return err
		}

		err = initCounter(vmctr, MaxVM, cb)
		if err != nil {
			return err
		}

		return nil

	})
	if err != nil {
		log.WithError(err).Fatal("failed to init db")
	}

}

func initBucket(name []byte, tx *bolt.Tx) (*bolt.Bucket, error) {

	var err error
	b := tx.Bucket(name)
	if b == nil {

		b, err = tx.CreateBucket(name)
		if err != nil {
			return nil, err
		}

	}

	return b, nil

}

func initCounter(name []byte, size int, b *bolt.Bucket) error {

	v := b.Get(name)
	if v == nil {

		cs := cogs.CountSet{Size: size}
		v, err := json.Marshal(cs)
		if err != nil {
			return err
		}

		err = b.Put(name, v)
		if err != nil {
			return err
		}

	}

	return nil
}

func SaveInstance(i *api.InstanceInfo) error {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("failed to open db")
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(ibucket)
		m := jsonpb.Marshaler{}
		var v bytes.Buffer
		err := m.Marshal(&v, i)
		if err != nil {
			log.WithError(err).Error("error marshalling instance")
			return err
		}

		err = b.Put([]byte(i.Spec.Name), v.Bytes())
		if err != nil {
			log.WithError(err).Error("error saving instance")
			return err
		}

		return nil

	})

	return nil

}

func DeleteInstance(i *api.InstanceInfo) error {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("failed to open db")
		return err
	}
	defer db.Close()

	return db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(ibucket)

		err = b.Delete([]byte(i.Spec.Name))
		if err != nil {
			log.WithError(err).Error("error deleting instance")
			return err
		}

		return nil

	})

	return nil

}

type InstanceInfos []*api.InstanceInfo

func (is *InstanceInfos) Save() error {

	for _, i := range *is {
		err := SaveInstance(i)
		if err != nil {
			return err
		}
	}

	return nil

}

func (is *InstanceInfos) Delete() error {

	var errs []error
	for _, i := range *is {
		err := DeleteInstance(i)
		if err != nil {
			errs = append(errs, err)
		}
	}

	return coalesce(errs)

}

func GetInstance(name string) (*api.InstanceInfo, error) {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("error opening db")
		return nil, err
	}
	defer db.Close()

	instance := &api.InstanceInfo{}

	err = db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket(ibucket)
		v := b.Get([]byte(name))
		if v == nil {
			return fmt.Errorf("not found")
		}

		err := jsonpb.Unmarshal(bytes.NewReader(v), instance)
		if err != nil {
			log.WithError(err).Error("error unmarshalling instance")
			return err
		}

		return nil

	})

	if err != nil {
		return nil, err
	}

	return instance, nil

}

func modVmIndex(
	vm *api.VMSpec,
	op func(cogs.CountSet) (cogs.CountSet, error)) error {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("failed to open db")
		return err
	}
	defer db.Close()

	vmctrMtx.Lock()
	defer vmctrMtx.Unlock()

	return db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(cbucket)
		var cs cogs.CountSet
		v := b.Get(vmctr)
		err := json.Unmarshal(v, &cs)
		if err != nil {
			log.WithError(err).Error("error unmarshalling vm count set")
			return err
		}

		cs, err = op(cs)
		if err != nil {
			return err
		}

		v, err = json.Marshal(cs)
		if err != nil {
			log.WithError(err).Error("countset searialization failed")
			return err
		}

		err = b.Put(vmctr, v)
		if err != nil {
			log.WithError(err).Error("countset writeback failed")
			return err
		}

		return nil

	})

}

func assignVmIndex(vm *api.VMSpec) error {

	return modVmIndex(vm,
		func(cs cogs.CountSet) (cogs.CountSet, error) {

			i, cs, err := cs.Add()
			if err != nil {
				log.WithError(err).Error("error incrementing count set")
				return cs, err
			}
			vm.Index = uint32(i)

			return cs, nil

		})

}

func freeVmIndex(vm *api.VMSpec) error {

	return modVmIndex(vm,
		func(cs cogs.CountSet) (cogs.CountSet, error) {

			cs = cs.Remove(int(vm.Index))
			return cs, nil

		})

}

func modInterfaces(
	vm *api.VMSpec,
	op func(cogs.CountSet, *api.EtherPort) (cogs.CountSet, error)) error {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("failed to open db")
		return err
	}
	defer db.Close()

	ifctrMtx.Lock()
	defer ifctrMtx.Unlock()

	return db.Update(func(tx *bolt.Tx) error {

		b := tx.Bucket(cbucket)
		var cs cogs.CountSet
		v := b.Get(ifxctr)
		err := json.Unmarshal(v, &cs)
		if err != nil {
			log.WithError(err).Error("error unmarshalling interface count set")
			return err
		}

		for _, nic := range vm.Nics {
			for _, port := range nic.Ports {

				cs, err = op(cs, port)
				if err != nil {
					return err
				}

			}
		}

		v, err = json.Marshal(cs)
		if err != nil {
			log.WithError(err).Error("countset searialization failed")
			return err
		}

		err = b.Put(ifxctr, v)
		if err != nil {
			log.WithError(err).Error("countset writeback failed")
			return err
		}

		return nil

	})

}

func assignInterfaces(vm *api.VMSpec) error {

	return modInterfaces(vm,
		func(cs cogs.CountSet, port *api.EtherPort) (cogs.CountSet, error) {

			i, cs, err := cs.Add()
			if err != nil {
				log.WithError(err).Error("error incrementing count set")
				return cs, err
			}
			if port.Ifx == nil {
				port.Ifx = new(api.Netif)
			}
			port.Ifx.Index = uint32(i)

			return cs, nil

		})

}

func freeInterfaces(vm *api.VMSpec) error {

	return modInterfaces(vm,
		func(cs cogs.CountSet, port *api.EtherPort) (cogs.CountSet, error) {

			cs = cs.Remove(int(port.Ifx.Index))
			return cs, nil

		})

}

func listInstances() (InstanceInfos, error) {

	db, err := bolt.Open(dbfile, 0600, nil)
	if err != nil {
		log.WithError(err).Error("error opening db")
		return nil, err
	}
	defer db.Close()

	var result InstanceInfos

	err = db.View(func(tx *bolt.Tx) error {

		b := tx.Bucket(ibucket)
		c := b.Cursor()

		var result InstanceInfos

		for k, v := c.First(); k != nil; k, v = c.Next() {

			instance := &api.InstanceInfo{}
			err := jsonpb.Unmarshal(bytes.NewReader(v), instance)
			if err != nil {
				log.WithError(err).WithFields(log.Fields{
					"key":   string(k),
					"value": string(v),
				}).Error("error unmarshalling instance")
				continue
			}

			result = append(result, instance)

		}

		return nil

	})

	if err != nil {
		return nil, err
	}

	return result, nil

}
