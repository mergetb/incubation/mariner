package mariner

import (
	"os"

	log "github.com/sirupsen/logrus"
)

const (
	MaxIfx = 1 << 16
	MaxVM  = 1 << 16
)

// override as necessary
var (
	VncListen    = "0.0.0.0"
	QemuEmulator = "qemu-system-x86_64"
	QemuVersion  = "3.1"
	QemuMachine  = "pc-q35-3.1"
	VncStart     = 1001 //relative to 5900
)

func RuntimeInit() {

	// base runtime initialization

	err := os.MkdirAll("/var/mariner/pid", 0755)
	if err != nil {
		log.WithError(err).Fatal("failed to create /var/mariner/pid dir")
	}

	err = os.MkdirAll("/var/mariner/bootdisk", 0755)
	if err != nil {
		log.WithError(err).Fatal("failed to create /var/mariner/bootdisk dir")
	}

	// follow-on subsystem initialization

	DbInit()
	NetInit()

}
