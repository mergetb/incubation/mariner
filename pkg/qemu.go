package mariner

import (
	"fmt"
	"io/ioutil"
	"os"
	"os/exec"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
	api "gitlab.com/mergetb/tech/mariner/api"
)

func create(vm *api.VMSpec) (*api.InstanceInfo, error) {

	fields := log.Fields{"name": vm.Name}

	log.WithFields(fields).Debug("creating vm")

	err := assignVmIndex(vm)
	if err != nil {
		return nil, err
	}

	fields["index"] = vm.Index
	log.WithFields(fields).Debug("got vm index")

	err = assignInterfaces(vm)
	if err != nil {
		return nil, err
	}

	err = createVmInterfaces(vm)
	if err != nil {
		return nil, err
	}

	err = createBootdisk(vm)
	if err != nil {
		return nil, err
	}

	cmd := NewQCmd(vm)
	out, err := cmd.CombinedOutput()
	if err != nil {

		log.WithError(err).WithFields(log.Fields{
			"name": vm.Name,
			"out":  string(out),
		}).Error("vm launch failed")

		return nil, err

	}

	return &api.InstanceInfo{Spec: vm}, nil

}

func destroy(instance *api.InstanceInfo) error {

	fields := log.Fields{
		"name": instance.Spec.Name,
	}

	var errs []error

	err := deleteVmInterfaces(instance.Spec)
	if err != nil {
		errs = append(errs, err)
	}

	err = freeInterfaces(instance.Spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Warn("error freeing interface counters, assuming freed")
		errs = append(errs, err)
	}

	err = freeVmIndex(instance.Spec)
	if err != nil {
		log.
			WithError(err).
			WithFields(fields).
			Warn("error freeing vm index, assuming freed")
		errs = append(errs, err)
	}

	pid, err := readPid(instance.Spec)
	if err != nil {

		log.
			WithError(err).
			WithFields(fields).
			Warn("error failed to read vm pid")
		errs = append(errs, err)

	} else {

		p, err := os.FindProcess(int(pid))
		if err != nil {
			log.
				WithError(err).
				WithFields(fields).
				Warn("error finding process, assuming dead")
			errs = append(errs, err)
			return coalesce(errs)
		}

		go func() {
			p.Wait()
		}()

		err = p.Kill()
		if err != nil {
			log.WithError(err).WithFields(fields).Error("error killing vm process")
			return err
		}

		err = os.RemoveAll(
			fmt.Sprintf("/var/mariner/pid/%s", instance.Spec.Name),
		)
		if err != nil {
			log.WithError(err).WithFields(fields).Error("error deleting pid file")
			return err
		}

		err = deleteBootdisk(instance.Spec)
		if err != nil {
			return err
		}

	}

	return coalesce(errs)
}

func readPid(vm *api.VMSpec) (int, error) {

	data, err := ioutil.ReadFile(
		fmt.Sprintf("/var/mariner/pid/%s", vm.Name),
	)
	if err != nil {
		return -1, err
	}

	pid, err := strconv.Atoi(strings.TrimSuffix(string(data), "\n"))
	if err != nil {
		return -1, err
	}

	return pid, nil

}

func destroyAll(instances InstanceInfos) []error {

	var errs []error

	for _, i := range instances {
		err := destroy(i)
		if err != nil {
			errs = append(errs, err)
		}
	}

	return errs

}

func createBootdisk(vm *api.VMSpec) error {

	//TODO user spec
	base := "/var/mariner/img/debian-buster"
	instance := fmt.Sprintf("/var/mariner/bootdisk/%s", vm.Name)

	out, err := exec.Command(
		"qemu-img",
		"convert",
		"-O", "qcow2",
		"-o", "lazy_refcounts=on,preallocation=falloc",
		base,
		instance,
	).CombinedOutput()

	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{
				"name": vm.Name,
				"out":  string(out),
			}).
			Error("error creating instance boot disk")
		return err
	}

	return nil

}

func deleteBootdisk(vm *api.VMSpec) error {

	err := os.RemoveAll(fmt.Sprintf("/var/mariner/bootdisk/%s", vm.Name))
	if err != nil {
		log.
			WithError(err).
			WithFields(log.Fields{"name": vm.Name}).
			Error("error deleting image boot disk")
		return err
	}

	return nil

}
