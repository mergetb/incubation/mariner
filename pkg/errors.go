package mariner

import (
	"fmt"
)

func coalesce(errs []error) error {

	if len(errs) == 0 {
		return nil
	}

	var msg string
	for _, err := range errs {
		msg += err.Error() + ";"
	}

	// -1 to remove trailing ;
	return fmt.Errorf(msg[:len(msg)-1])

}
