package main

import (
	"context"
	"testing"

	"google.golang.org/grpc"

	api "gitlab.com/mergetb/tech/mariner/api"
)

func TestVmUp(t *testing.T) {

	conn, client := connect(t)
	defer conn.Close()

	_, err := client.CreateVMs(context.TODO(), &api.CreateVMsRequest{
		Specs: []*api.VMSpec{
			&api.VMSpec{
				Name: "murphy",
			},
		},
	})
	if err != nil {
		t.Fatalf("CreateVMsRequest error: %v", err)
	}

}

func TestVmDown(t *testing.T) {

	conn, client := connect(t)
	defer conn.Close()

	_, err := client.DestroyVMs(context.TODO(), &api.DestroyVMsRequest{
		Names: []string{"murphy"},
	})
	if err != nil {
		t.Fatalf("CreateVMsRequest error: %v", err)
	}

}

func TestNetVmUp(t *testing.T) {

	conn, client := connect(t)
	defer conn.Close()

	_, err := client.CreateVMs(context.TODO(), &api.CreateVMsRequest{
		Specs: []*api.VMSpec{
			&api.VMSpec{
				Name: "murphy",
				Nics: []*api.EtherNic{{
					Ports: []*api.EtherPort{{
						Ifx: &api.Netif{
							Vlan: 10,
						},
						Capacity: 1e9,
					}},
				}},
			},
			&api.VMSpec{
				Name: "abby",
				Nics: []*api.EtherNic{{
					Ports: []*api.EtherPort{{
						Ifx: &api.Netif{
							Vlan: 10,
						},
						Capacity: 1e9,
					}},
				}},
			},
		},
	})
	if err != nil {
		t.Fatalf("CreateVMsRequest error: %v", err)
	}

}

func TestNetVmIsoUp(t *testing.T) {

	conn, client := connect(t)
	defer conn.Close()

	_, err := client.CreateVMs(context.TODO(), &api.CreateVMsRequest{
		Specs: []*api.VMSpec{
			&api.VMSpec{
				Name: "murphy",
				Nics: []*api.EtherNic{{
					Ports: []*api.EtherPort{{
						Ifx: &api.Netif{
							Vlan: 10,
						},
						Capacity: 1e9,
					}},
				}},
			},
			&api.VMSpec{
				Name: "abby",
				Nics: []*api.EtherNic{{
					Ports: []*api.EtherPort{{
						Ifx: &api.Netif{
							Vlan: 11,
						},
						Capacity: 1e9,
					}},
				}},
			},
		},
	})
	if err != nil {
		t.Fatalf("CreateVMsRequest error: %v", err)
	}

}

func TestNetVmDown(t *testing.T) {

	conn, client := connect(t)
	defer conn.Close()

	_, err := client.DestroyVMs(context.TODO(), &api.DestroyVMsRequest{
		Names: []string{"murphy", "abby"},
	})
	if err != nil {
		t.Fatalf("CreateVMsRequest error: %v", err)
	}

}

func connect(t *testing.T) (*grpc.ClientConn, api.MarinerClient) {

	conn, err := grpc.Dial("localhost:1995", grpc.WithInsecure())
	if err != nil {
		t.Fatalf("dial error: %v", err)
	}

	client := api.NewMarinerClient(conn)

	return conn, client
}
