package main

import (
	"log"

	"gitlab.com/mergetb/tech/mariner/pkg"
)

func main() {

	log.SetFlags(0)

	log.Printf("mariner version %s", mariner.Version)

}
