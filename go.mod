module gitlab.com/mergetb/tech/mariner

go 1.12

require (
	github.com/golang/protobuf v1.3.1
	github.com/sirupsen/logrus v1.4.2
	gitlab.com/mergetb/tech/cogs v0.2.18
	gitlab.com/mergetb/tech/rtnl v0.1.6
	go.etcd.io/bbolt v1.3.3
	google.golang.org/grpc v1.21.1
)
