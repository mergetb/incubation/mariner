all: build/mariner build/marinerd

build:
	$(QUIET) mkdir build

go.mod go.sum:
	go mod init gitlab.com/mergetb/tech/mariner

.tools:
	$(QUIET) mkdir .tools

VERSION = $(shell git describe --always --long --dirty)
LDFLAGS = "-X gitlab.com/mergetb/tech/mariner/pkg.Version=$(VERSION)"

.PHONY: clean
clean:
	$(QUIET) rm -rf build

protoc-gen-go=.tools/protoc-gen-go
$(protoc-gen-go): go.mod | .tools
	$(QUIET) GOBIN=`pwd`/.tools go install github.com/golang/protobuf/protoc-gen-go

api/spec.pb.go: api/spec.proto $(protoc-gen-go)
	$(protoc-build)
	$(QUIET) sed -r -i 's/json:"(.*)"/json:"\1" yaml:"\1" mapstructure:"\1"/g' api/spec.pb.go

_PKGSRC = cmd.go db.go errors.go net.go qemu.go runtime.go version.go vm.go
PKGSRC = $(addprefix pkg/,$(_PKGSRC))

_APISRC = spec.pb.go
APISRC = $(addprefix api/,$(_APISRC))

build/mariner: cmd/mariner/main.go go.mod $(APISRC) $(PKGSRC) | build
	$(go-build)

build/marinerd: svc/marinerd/main.go go.mod $(APISRC) $(PKGSRC) | build
	$(go-build)

BLUE=\e[34m
GREEN=\e[32m
CYAN=\e[36m
NORMAL=\e[39m

QUIET=@
DOCKER_QUIET=-q
ifeq ($(V),1)
	QUIET=
	DOCKER_QUIET=
endif

define build-slug
	@echo "$(BLUE)$1$(GREEN)\t $< $(CYAN)$@$(NORMAL)"
endef

define go-build
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $(dir $<)/*
endef

define go-build-file
	$(call build-slug,go)
	$(QUIET) go build -ldflags=$(LDFLAGS) -o $@ $<
endef

define protoc-build
	$(call build-slug,protoc)
	$(QUIET) PATH=./.tools:$$PATH protoc \
		-I . \
		-I ./$(dir $@) \
		./$< \
		--go_out=plugins=grpc:.
endef
