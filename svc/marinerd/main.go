package main

import (
	"context"
	"flag"
	"net"

	log "github.com/sirupsen/logrus"
	"google.golang.org/grpc"

	api "gitlab.com/mergetb/tech/mariner/api"
	"gitlab.com/mergetb/tech/mariner/pkg"
)

var (
	endpoint = flag.String("endpoint", "0.0.0.0:1995", "endpoint to listen on")
	debug    = flag.Bool("debug", false, "turn on debugging")
)

type svc struct{}

func main() {

	libFlags()

	if *debug {
		log.SetLevel(log.DebugLevel)
	}

	mariner.RuntimeInit()

	log.Printf("marinerd version %s\n", mariner.Version)

	server := grpc.NewServer()
	api.RegisterMarinerServer(server, &svc{})

	l, err := net.Listen("tcp", *endpoint)
	if err != nil {
		log.Fatal("failed to listen: %#v", err)
	}

	log.Infof("Listening on tcp://%s", *endpoint)
	server.Serve(l)

}

// flags to override mariner defaults if desired
func libFlags() {

	flag.StringVar(&mariner.VncListen, "vnc-listen", mariner.VncListen,
		"address to lisetn for vnc connections on")

	flag.StringVar(&mariner.QemuEmulator, "qemu-emulator", mariner.QemuEmulator,
		"QEMU emulator binary to use")

	flag.StringVar(&mariner.QemuVersion, "qemu-version", mariner.QemuVersion,
		"local QEMU version")

	flag.StringVar(&mariner.QemuMachine, "qemu-machine", mariner.QemuMachine,
		"QEMU machine type to use")

	flag.Parse()

}

// vms ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

func (s *svc) CreateVMs(
	ctx context.Context, req *api.CreateVMsRequest,
) (*api.CreateVMsResponse, error) {

	err := mariner.CreateVMs(req.Specs)
	if err != nil {
		return nil, err
	}

	return &api.CreateVMsResponse{}, nil

}

func (s *svc) DestroyVMs(
	ctx context.Context, req *api.DestroyVMsRequest,
) (*api.DestroyVMsResponse, error) {

	err := mariner.DestroyVMs(req.Names)
	if err != nil {
		return nil, err
	}

	return &api.DestroyVMsResponse{}, nil

}

func (s *svc) ListVMs(
	ctx context.Context, req *api.ListVMsRequest,
) (*api.ListVMsResponse, error) {

	specs, err := mariner.ListVMs()
	if err != nil {
		return nil, err
	}

	return &api.ListVMsResponse{Specs: specs}, nil

}
